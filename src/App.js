import React, { Component } from "react";
import Homepage from "./HomePage";
import Profile from "./components/Profile";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CompanyList from "./components/CompanyList";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/profile" component={Profile}></Route>
          <Route path="/companies" component={CompanyList}></Route>
          <Route path="/" component={Homepage}></Route>
        </Switch>
      </Router>
    );
  }
}
