import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

export default function Header() {
  return (
    <div
      className="ui"
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: "20px",
        paddingRight: "20px",
        paddingTop: "10px",
        paddingBottom: "10px",
        backgroundColor: "white",
      }}
    >
      <div className="ui two item right" style={{ marginLeft: "1100px" }}>
        <Link to="/profile">
          <Button className="item" style={{ marginLeft: "10px" }}>
          Profile
          </Button>
        </Link>
        <Link to="/companies">
          <Button className="item">See Companies</Button>
        </Link>
      </div>
    </div>
  );
}
