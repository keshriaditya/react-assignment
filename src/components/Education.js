import React, { useState } from "react";
import EducationForm from "./EducationForm";
import { Button } from "semantic-ui-react";
export default function Education() {
  const [educationdetails, setEducationdetails] = useState([]);

  const details = (e) => {
    setEducationdetails([...educationdetails, e]);
  };

  const removeBtn = (degreename) => {
    const removedata = educationdetails.filter(
      (degree) => degree.degree !== degreename
    );
    setEducationdetails(removedata);
  };

  return (
    <>
      <div>
        <EducationForm educationdetails={details} />
      </div>
      {educationdetails &&
        educationdetails.map((e, index) => (
          <div
            style={{
              marginLeft: "2.5rem",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "70%",
              marginBottom: "2rem",
              marginTop: "1rem",
            }}
            key={index}
          >
            <div className="content">
              <h5
                style={{
                  fontSize: "15px",
                  fontWeight: "800",
                  textTransform: "capitalize",
                  color: "teal",
                }}
              >
                {e.degree}
              </h5>
              <h5>{e.college}</h5>
              <h5>{e.location}</h5>
              <h5>
                {e.monthfrom} {e.yearfrom} to {e.monthto} {e.yearto}
              </h5>
            </div>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => removeBtn(e.degree)}
            >
              Remove
            </Button>
          </div>
        ))}

      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          position: "fixed",
          bottom: "2rem",
          right: "1rem",
          width: "20%",
        }}
      ></div>
    </>
  );
}
