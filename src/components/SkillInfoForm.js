import React, { useState } from "react";

export default function SkillinfoForm({ skilldetails }) {
  const [hidden, setHidden] = useState(true);
  const [skillInfo, setSkillInfo] = useState({
    skill: "",
    rating: "",
  });

  const formSubmit = (e) => {
    e.preventDefault();

    skilldetails(skillInfo);
    setSkillInfo({
      skill: "",
      rating: "",
    });
  };

  return (
    <div style={{ marginTop: "10px" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          textAlign: "center",
        }}
      >
        <h3>Skills</h3>
        {hidden ? (
          <button
            className="ui button primary"
            onClick={() => setHidden(!hidden)}
          >
            Add
          </button>
        ) : (
          <button className="ui button red" onClick={() => setHidden(!hidden)}>
            Close
          </button>
        )}
      </div>

      <form className="ui form  segment" onSubmit={formSubmit} hidden={hidden}>
        <div className="field">
          <label>Job Title</label>
          <input
            type="text"
            name="skill"
            onChange={(e) =>
              setSkillInfo({ ...skillInfo, skill: e.target.value })
            }
          />
        </div>
        <div className="field">
          <label>Rating</label>
          <input
            type="number"
            min="0"
            max="10"
            name="rating"
            onChange={(e) =>
              setSkillInfo({ ...skillInfo, rating: e.target.value })
            }
          />
        </div>

        <button className="ui button primary" type="submit">
          Save
        </button>
      </form>
    </div>
  );
}
