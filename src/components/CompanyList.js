import React, { Component } from "react";
import CompanyDetail from "./CompanyDetail";
import CompanySelect from "./CompanySelect";
import Header from "./Header";
export default class CompanyList extends Component {
  render() {
    return (
      <div style={{ marginLeft: "40px", marginRight: "40px" }}>
        <Header />

        <CompanySelect />
        <CompanyDetail />
      </div>
    );
  }
}
