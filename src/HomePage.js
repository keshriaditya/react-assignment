import React, { Component } from "react";
import Jobs from "./jobs.json";
import JobBreifList from "./JobBreifList";
import "./App.css";
import Header from "./components/Header";

export default class Homepage extends Component {
  state = { searchJob: "", searchCity: "", isLoading: true };
  componentDidMount() {
    this.timerHandle = setTimeout(
      () => this.setState({ isLoading: false }),
      6000
    );
  }

  render() {
    const { searchJob, searchCity, isLoading } = this.state;
    const filteredJobs = Jobs.filter(
      (Job) =>
        Job.name.toLowerCase().includes(searchJob.toLowerCase()) &&
        Job.location.city.toLowerCase().includes(searchCity.toLowerCase())
    );
    if (isLoading) {
      return <div className="loader"></div>;
    } else {
      return (
        <div className="App"
        >
          <Header />
          <img
            src="https://picsum.photos/id/180/500/200"
            className="company-banner"
            alt="banner"
          />

          <div className="container ">
            <div className="searchField ">
              <div class="ui labeled input">
                <div class="ui label brown">What</div>
                <input
                  type="text"
                  placeholder="Job title, keywords or company "
                  value={this.state.searchJob}
                  autoFocus
                  onChange={(e) => this.setState({ searchJob: e.target.value })}
                />
              </div>
              <div class="ui labeled input">
                <div class="ui label grey">Where</div>
                <input
                  type="text"
                  placeholder="Enter City"
                  value={this.state.searchCity}
                  onChange={(e) =>
                    this.setState({ searchCity: e.target.value })
                  }
                />
              </div>
              <button className="ui blue button">Find Jobs</button>
            </div>
          </div>

          <div className="JobsView"
          >
            <JobBreifList jobs={filteredJobs} />
          </div>
        </div>

      );
    }
  }
}
